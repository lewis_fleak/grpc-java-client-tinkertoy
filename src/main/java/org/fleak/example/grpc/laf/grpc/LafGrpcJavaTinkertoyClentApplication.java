package org.fleak.example.grpc.laf.grpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class LafGrpcJavaTinkertoyClentApplication {

	public static void main(String[] args) {
		SpringApplication.run(LafGrpcJavaTinkertoyClentApplication.class, args);
	}

}
