package org.fleak.example.grpc.laf.grpc.config;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldServiceGrpc;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloWorldGrpcClientConfig {

    @Bean
    public HelloWorldServiceGrpc.HelloWorldServiceBlockingStub helloWorldGrpcClient() {

        // creating a network channel to the server
        ManagedChannel helloWorldChannel =
                ManagedChannelBuilder.forAddress("localhost", 9090)
                        .usePlaintext()
                        .build();

        // binding the network channel to the client object
        return HelloWorldServiceGrpc.newBlockingStub(helloWorldChannel);
    }

}
