package org.fleak.example.grpc.laf.grpc.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldGreeting;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldResponse;
import org.fleak.example.grpc.laf.grpc.server.HelloWorldServiceGrpc.HelloWorldServiceBlockingStub;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class HelloWorldClientService {

    private final HelloWorldServiceBlockingStub helloWorldServiceClient;

    @Scheduled(fixedDelay = 5000)
    public void pingHelloWorldServer() {
        HelloWorldGreeting hailRequest = buildHelloWorldGreeting("Mary", "Hadalamb");

        Instant rpcStart = Instant.now();

        // invoke the gRPC client
        HelloWorldResponse hailResponse = helloWorldServiceClient.hail(hailRequest);

        Instant rpcEnd = Instant.now();
        Duration rpcElapsedTime = Duration.between(rpcStart, rpcEnd);

        final Instant responseTime =
                Instant.ofEpochSecond(hailResponse.getResponseTime().getSeconds(), hailResponse.getResponseTime().getNanos());

        log.info("gRPC response:  {}", hailResponse.getResponse());
        log.info("gRPC Times | start:  {} | end:  {} | elapsed:  {} microseconds",
                rpcStart, rpcEnd, TimeUnit.NANOSECONDS.toMicros(rpcElapsedTime.getNano()));
    }

    private HelloWorldGreeting buildHelloWorldGreeting(String firstName, String lastName) {
        return HelloWorldGreeting.newBuilder()
                .setFirstName(firstName)
                .setLastName(lastName)
                .build();
    }

}
